class ErrorController {
    getPageNotFound(req, res, next){
        res.status(404).render('page-not-found', { pageTitle: 'Not Found' });
    }
}

module.exports = new ErrorController();
