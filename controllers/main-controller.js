class MainController {
    getMainPage(req, res, next) {
        res.render('shop/index', { pageTitle: 'Main Page' });
    }
}

module.exports = new MainController();
