const cartService = require('../services/cart-service');

class CartController {
    getCartPage(req, res, next) {
        cartService.getCart(req.session.userId)
            .then(cart => res.render('shop/cart', { pageTitle: 'Cart', cart }))
            .catch(err => next(new Error(err)));
    }

    addProductToCart(req, res, next) {
        cartService.addToCart(req.session.userId, req.body.productId)
            .then(() => res.redirect('/cart'))
            .catch(err => next(new Error(err)));
    }

    deleteProductFromCart(req, res, next) {
        cartService.deleteFromCart(req.session.userId, req.body.productId)
            .then(() => res.redirect('/cart'))
            .catch(err => next(new Error(err)));
    }
}

module.exports = new CartController();
