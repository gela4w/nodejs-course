const productService = require('../services/product-service');

class ShopController {
    getAllProducts(req, res, next) {
        productService.getAllProducts(null)
            .then(products => {
                res.render('shop/product-list', { pageTitle: 'My products', products })
            })
            .catch(err => next(new Error(err)));
    }

    getProductDetails(req, res, next) {
        productService.getProductById(req.params.id)
            .then(product =>  {
                res.render('shop/product-detail', { pageTitle: product.title, product })
            })
            .catch(err => next(new Error(err)));
    }
}

module.exports = new ShopController();
