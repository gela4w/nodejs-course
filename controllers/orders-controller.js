const ordersService = require('../services/orders-service');
const User = require('../database/models/user');

class OrdersController {
    getOrdersPage(req, res, next) {
        const { userId } = req.session;
        let userName = '';

        return User.findById(userId).then(({ email }) => {
            userName = email;

            return ordersService.getOrders(userId).then(orders => {
                        res.render('shop/orders', { pageTitle: 'Orders', orders, userName });
                    })
            })
            .catch(err => next(new Error(err)));
    }

    createOrder(req, res, next) {
        ordersService.createOrder(req.session.userId)
            .then(() => res.redirect('/orders'))
            .catch(err => next(new Error(err)));
    }

    getInvoice(req, res, next) {
        const orderId = req.params.orderId;
        const invoiceName = `invoice_${orderId}.pdf`;

        ordersService.getInvoice(orderId, req.session.userId)
            .then(invoice => {
                if (!invoice) {
                    return res.redirect('/orders');
                }

                res.setHeader('Content-Type', 'application/pdf');
                res.setHeader('Content-Disposition', 'inline; filename="' + invoiceName + '"');
                invoice.pipe(res);
            })
            .catch(err => next(err));
    }
}

module.exports = new OrdersController();
