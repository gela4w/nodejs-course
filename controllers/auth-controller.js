const { validationResult } = require('express-validator');

const authService = require('../services/auth-service');
const mailerService = require('../services/mailer-service');

const getErrorMessage = (req) => {
    let errorMessage = req.flash('error');
    return errorMessage.length ? errorMessage[0] : null;
};

class AuthController {
    getLoginPage(req, res, next) {
        res.render('auth/login', {
            pageTitle: 'Login',
            errorMessage: '',
            userInput: { email: '', password: '' },
        });
    }

    login(req, res, next) {
        const { email, password } = req.body;
        const errors = validationResult(req).array();

        if (errors.length) {
            return res.status(422).render('auth/login', {
                    pageTitle: 'Login',
                    errorMessage: errors[0].msg,
                    userInput: { email, password }
                });
        }

        authService.login(email, password).then(user => {
            if (user) {
                req.session.userId = user._id;
                req.session.save(err => {
                    if(!err) { res.redirect('/'); }
                })
            } else {
                res.status(422).render('auth/login', {
                    pageTitle: 'Login',
                    errorMessage: 'Invalid user email or password.',
                    userInput: { email, password }
                });
            }
        })
    }

    logout(req, res, next) {
        req.session.destroy(err => {
            if(!err) { res.redirect('/login'); }
        });
    }

    getSignupPage(req, res, next) {
        res.render('auth/signup', {
            pageTitle: 'Signup',
            errorMessage: '',
            userInput: { email: '', password: '', confirmPassword: '' }
        });
    }

    signup(req, res, next) {
        const { email, password, confirmPassword } = req.body;
        const errors = validationResult(req).array();

        if (errors.length) {
            return res.status(422).render('auth/signup', {
                pageTitle: 'Signup',
                errorMessage: errors[0].msg,
                userInput: { email, password, confirmPassword }
            });
        }

        authService.signup(email, password).then(() => {
                res.redirect('/login');
                return mailerService.sendEmail(
                    email,
                    'Signup succeeded',
                    `<h1>You have successfully signed up!</h1>`);
            })
            .catch(err => next(new Error(err)));
    }

    getResetPage(req, res, next) {
        res.render('auth/reset-password', { pageTitle: 'Reset Password', errorMessage: getErrorMessage(req) });
    }

    resetPassword(req, res, next) {
        authService.resetPassword(req.body.email).then(user => {
                if (!user) {
                    req.flash('error', 'No account with this email was found.');
                    return res.redirect('/reset-password');
                }

                res.redirect('/');
                return mailerService.sendEmail(
                    user.email,
                    'Password reset',
                    `<p>You have requested a password reset.</p>
                           <p>Click this
                              <a href="http://localhost:3000/new-password/${user.resetToken}">link</a>
                           to set a new password.</p>`);
        })
            .catch(err => next(new Error(err)));
    }

    getNewPasswordPage(req, res, next) {
        const resetToken = req.params.token;

        authService.getUserByResetToken(resetToken).then(user => {
            if (!user) {
                console.log('No user with such reset password token');

                return res.redirect('/page-not-found');
            }

            res.render('auth/new-password', {
                pageTitle: 'New Password',
                errorMessage: getErrorMessage(req),
                userId: user._id.toString(),
                resetToken
            });
        })
            .catch(err => next(new Error(err)));
    }

    setNewPassword(req, res, next) {
        const { userId, resetToken, password } = req.body;

        authService.setNewPassword(userId, resetToken, password).then(() => {
            req.flash('message', 'You have successfully updated your password.')
            res.redirect('/login');
        })
            .catch(err => next(new Error(err)));
    }
}

module.exports = new AuthController();
