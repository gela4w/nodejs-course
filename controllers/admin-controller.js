const { validationResult } = require('express-validator');

const productService = require('../services/product-service');
const cartService = require('../services/cart-service');

class AdminController {
    getAllProducts(req, res, next) {
        //Authorization: let modify product only to its creator
        productService.getAllProducts(req.session.userId)
            .then(products => res.render('admin/products', { pageTitle: 'Products', products }))
            .catch(err => next(new Error(err)));
    }

    getProductForm(req, res, next) {
        productService.getDataForForm(req.params.id).then(({ pageTitle, product, formAction }) =>
            res.render('admin/product-form', { pageTitle, product, formAction, errorMessage: '' }))
            .catch(err => next(new Error(err)));
    }

    addProduct(req, res, next) {
        const validationErrors = validationResult(req).array();

        if (validationErrors.length) {
            return res.status(422).render('admin/product-form', {
                pageTitle: 'Add product',
                product: req.body,
                formAction: '/admin/add-product',
                errorMessage: validationErrors[0].msg
            });
        }

        if (!req.file) {
            return res.status(422).render('admin/product-form', {
                pageTitle: 'Add product',
                product: req.body,
                formAction: '/admin/add-product',
                errorMessage: 'Uploaded file is not an image. Please select another one.'
            });
        }

        const product = { ...req.body, file: req.file };

        productService.addProduct(product, req.session.userId)
            .then(() => res.redirect('/admin/products'))
            .catch(err => next(new Error(err)));
    }

    editProduct(req, res, next) {
        const validationErrors = validationResult(req).array();

        if (validationErrors.length) {
            return res.status(422).render('admin/product-form', {
                pageTitle: `Edit ${req.body.title}`,
                product: { ...req.body, _id: req.body.productId },
                formAction: '/admin/edit-product',
                errorMessage: validationErrors[0].msg
            })
        }

        const product = { ...req.body, file: req.file };

        productService.editProduct(product, req.session.userId)
            .then(product => {
                product ? res.redirect('/admin/products') : res.redirect('/');
            })
            .catch(err => next(new Error(err)));
    }

    deleteProduct(req, res, next) {
        const productId = req.body.productId;

        cartService
            .deleteFromCart(req.session.userId, productId)
            .then(() => productService.deleteProduct(productId))
            .then(() => res.redirect('/admin/products'))
            .catch(err => next(new Error(err)));
    }
}

module.exports = new AdminController();
