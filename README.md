Notes:

    //create an express app
const app = express();

    //app.set() => sets an app global configuration value
    //Templating Engines are used to insert dynamic content to the html pages
    //Pug (Jade) => minimalistic HTML + allows to use JS directly
    //Handlebars => min JS, specific syntax
    //EJS => regular HTML, JS features
app.set('view engine', 'ejs');      //tells express to use ejs engine, when it tries to render dynamic templates
app.set('views', 'folder name');    //tells express where to find dynamic templates: 'views' --- default folder

    //middlewares are the logic that will be applied to the EVERY req/res (if no excluding logic is defined)
        //middleware gets the path(s) for the first arg => the order is IMPORTANT
        //if the root path ('/') will be the first,
        //it will never get into the second middleware,
        //because paths are checked by "contains" (NB: for 'use' method!!), and every path starts with (contains) the '/'
app.use((req,res,next) => {
//logic here...

    //should call next() to pass the "stream" further or call the res...() to send the response
    next();
});

app.use((req, res, next) => {
//some more logic...

    next();
});

    //this middleware allows to load files statically, i.e. says the app NOT TO FIND THE FILE WITHIN THE ROUTER
    //but load it directly from the file system
    //we use it for css/images/...
    //it accepts as an arg a path to the folder that servers static files
app.use(express.static(path.join(__dirname, 'public')));

    //register routers, that handle different routs
app.use('/products', productRouter);
app.use('/main', mainRouter);

    //res comes throughout all middlewares, and if it was not previously handled by any of them
    //here the 404-page will be shown
app.use((req, res, next) => {
res.status(404).sendFile(path.join(__dirname, 'views', 'not-found-page.html'));
})
