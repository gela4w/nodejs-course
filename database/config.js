const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const connectToMongo = cb => {
    //...net/SHOP --- the name of the db
    //if it does not exist --- will create it on the fly
    //the same with collections
    MongoClient
        .connect('mongodb+srv://gela:GDAseMXCUoJdm7QE@cluster0.ddblpgh.mongodb.net/shop?retryWrites=true&w=majority')
        .then(client => {
            console.log('Connected to Mongo');
            _db = client.db();

            cb();
        })
        .catch(err => {
            console.log(err);
            throw err;
        });
}

const getDb = () => {
    if(!_db) { throw 'There is no connection to the data base' }

    return _db;
}

exports.connectToMongo = connectToMongo;
exports.getDb = getDb;
