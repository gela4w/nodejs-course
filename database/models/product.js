const { Schema, model } = require('mongoose');

const productSchema = new Schema({
    title: {
        type: String,
        require: true
    },
    imageUrl: {
        type: String,
        require: true
    },
    price: {
        type: Number,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

//model() connects the schema with a collection name
//e.g., Product => products collection
module.exports = model('Product', productSchema);
