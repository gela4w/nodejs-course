const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    cart: [{
        productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
        amount: { type: Number, required: true }
    }],
    resetToken: String,
    resetTokenExpiration: Date
});

module.exports = model('User', userSchema);
