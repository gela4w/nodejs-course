const { Schema, model } = require('mongoose');

const orderSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    order: {
        products: [{ type: Object, required: true }],
        totalPrice: { type: Number, required: true }
    }
});

module.exports = model('Order', orderSchema);
