const { body } = require('express-validator');

module.exports = [
    //trim() --> a sanitizing func, that trim extra spaces from field before saving it to DB
    body('title')
        .isLength({ min: 3, max: 50 }).withMessage('Product title should contain from 3 to 50 characters.')
        .isString().trim(),
    body('price').isFloat(),
    body('description').isLength({ min: 5, max: 100 })
        .withMessage('Product description should contain from 5 to 100 characters.')
        .trim(),
];
