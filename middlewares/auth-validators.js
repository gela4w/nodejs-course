const { body } = require('express-validator');
const authService = require('../services/auth-service');

const loginValidators = [
    body('email')
        .isEmail().withMessage('Please enter a valid email.')
        .custom((value) => {
            return authService.getUserByEmail(value).then(user => {
                if (!user) {
                    return Promise.reject('Invalid user email or password.');
                }
            })
        }),
    body('password', 'Invalid user email or password.')
        .isLength({ min: 5 }).isAlphanumeric()
];

const signupValidators = [
    //checks fields for validation ONLY in request's body
    body('email')
        .isEmail().withMessage('Please enter a valid email.')
        .custom((value) => {
            return authService.getUserByEmail(value).then(user => {
                if (user) {
                    return Promise.reject('An account with this email already exists.');
                }
            })
        }),
    //set the custom error message to every validation in chain
    body('password', 'Password should contain only letters and numbers and be no shorter than 5 characters.')
        .isLength({ min: 5 }).isAlphanumeric(),
    body('confirmPassword').custom((value, { req }) => {
        if (value !== req.body.password) {
            throw new Error('Passwords do not match.')
        }

        return true;
    })
];

module.exports = { loginValidators, signupValidators };
