const { diskStorage } = require('multer');

const storage = diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images');
    },
    filename: (req, file, cb) => {
        const date = new Date();
        const prefix = `${date.getMonth() + 1}_${date.getDate()}_${date.getHours()}_${date.getMinutes()}`;
        cb(null, `${prefix}_${file.originalname}`);
    }
});

const fileFilter = (req, file, cb) => {
    const acceptedTypes = ['image/jpeg', 'image/jpg', 'image/png'];
    if (acceptedTypes.includes(file.mimetype)) {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

module.exports = { storage, fileFilter };
