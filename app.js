const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf')();
const flash = require('connect-flash');

const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');

const { storage, fileFilter } = require('./configs/multer');

const adminRouter = require('./router/admin');
const cartRouter = require('./router/cart');
const shopRouter = require('./router/shop');
const ordersRouter = require('./router/orders');
const authRouter = require('./router/auth');
const mainRouter = require('./router/main');

const errorController = require('./controllers/error-controller');

const app = express();
const store = new MongoDBStore({
    uri: process.env.CONNECTION_STRING,
    collection: 'sessions'
});

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }));
//multer is a parser for multipart form data (while bodyParser - for text only)
app.use(multer({ storage, fileFilter }).single('image'));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store
}));

//CSRF protection middleware:
//adds a csrfToken() function to every req =>
//allows us to generate token =>
//that should be embedded into rendered pages that can mutate app state (non-GET http methods)
app.use(csrf);

//The flash is a special area of the session used for storing messages.
// Messages are written to the flash and cleared after being displayed to the user.
// The flash is typically used in combination with redirects,
// ensuring that the message is available to the next page that is to be rendered.
app.use(flash());

app.use((req, res, next) => {
    res.locals.isAuthenticated = req.session.userId;
    res.locals.csrfToken = req.csrfToken();
    next();
})

app.use('/admin', adminRouter);
app.use('/cart', cartRouter);
app.use('/shop', shopRouter);
app.use('/orders', ordersRouter);
app.use(authRouter);
app.use(mainRouter);

app.use(errorController.getPageNotFound);

//"special" error handling middleware: accepts the 4th argument - an error
app.use((error, req, res, next) => {
    console.log(error);
    res.status(500).render('server-error-page', { pageTitle: 'Server error' });
})

mongoose
    .connect(process.env.CONNECTION_STRING)
    .then(() => {
        console.log('Connected to the MongoDB Server');
        app.listen(3000);
    })
    .catch(err => console.log(err));
