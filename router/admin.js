const express = require('express');

const adminController = require('../controllers/admin-controller');
const isAuthenticated = require('../middlewares/is-authenticated');
const productValidators = require('../middlewares/product-validators');

const router = express.Router();

router.get('/product-form', isAuthenticated, adminController.getProductForm);
router.get('/product-form/:id', isAuthenticated, adminController.getProductForm);
router.get('/products', isAuthenticated, adminController.getAllProducts);
router.post('/add-product', isAuthenticated, ...productValidators, adminController.addProduct);
router.post('/edit-product', isAuthenticated, ...productValidators, adminController.editProduct);
router.post('/delete-product', isAuthenticated, adminController.deleteProduct);

module.exports = router;
