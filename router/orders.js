const express = require('express');

const ordersController = require('../controllers/orders-controller');
const isAuthenticated = require('../middlewares/is-authenticated');

const router = express.Router();

router.get('/', isAuthenticated, ordersController.getOrdersPage);
router.get('/:orderId', isAuthenticated, ordersController.getInvoice);
router.post('/create', isAuthenticated, ordersController.createOrder);

module.exports = router;
