const express = require('express');

const cartController = require('../controllers/cart-controller');
const isAuthenticated = require('../middlewares/is-authenticated');

const router = express.Router();

router.get('/', isAuthenticated, cartController.getCartPage);
router.post('/add', isAuthenticated, cartController.addProductToCart);
router.post('/delete', isAuthenticated, cartController.deleteProductFromCart);

module.exports = router;
