const express = require('express');

const shopController = require('../controllers/shop-controller');

const router = express.Router();

router.get('/products', shopController.getAllProducts);
router.get('/product-details/:id', shopController.getProductDetails);

module.exports = router;
