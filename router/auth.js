const express = require('express');

const authController = require('../controllers/auth-controller');
const { loginValidators, signupValidators} = require('../middlewares/auth-validators');

const router = express.Router();

router.get('/login', authController.getLoginPage);
router.get('/signup', authController.getSignupPage);
router.get('/reset-password', authController.getResetPage);
router.get('/new-password/:token', authController.getNewPasswordPage);
router.post('/login', ...loginValidators, authController.login);
router.post('/logout', authController.logout);
router.post('/signup', ...signupValidators, authController.signup);
router.post('/reset-password', authController.resetPassword);
router.post('/new-password', authController.setNewPassword);

module.exports = router;
