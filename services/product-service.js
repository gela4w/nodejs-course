const Product = require('../database/models/product');
const deleteFile = require('../utils/delete-file');

class ProductService {

    getAllProducts(userId) {
        return userId ? Product.find({ userId }) : Product.find();
    }

    getProductById(id) {
        return Product.findById(id);
    }

    addProduct(product, userId) {
        const data = { ...product, imageUrl: product.file.path };
        return new Product({ ...data, userId }).save();
    }

    editProduct(updatedProduct, userId) {
        return Product.findById(updatedProduct.productId).then(product => {
            if (!product.userId.equals(userId)) {
                return null;
            }

            if (updatedProduct.file) {
                deleteFile(product.imageUrl);
                updatedProduct.imageUrl = updatedProduct.file.path;
            }

            Object.assign(product, updatedProduct);

            return product.save();
        });
    }

    deleteProduct(_id) {
        return Product.findById(_id).then(product => {
            if (!product) {
                throw Error('No such product.');
            }
            deleteFile(product.imageUrl);
        }).then(() => Product.deleteOne({ _id }));
    }

    getDataForForm(id) {
        if(id) {
            return this.getProductById(id).then(product => {
                return ({ pageTitle: `Edit ${product.title}`, product, formAction: '/admin/edit-product' })
            })
        } else {
            return Promise.resolve(({ pageTitle: 'Add product', product: new Product(), formAction: '/admin/add-product' }));
        }
    }
}

module.exports = new ProductService();
