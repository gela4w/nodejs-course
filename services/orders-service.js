const fs = require('fs');
const path = require('path');
const PDFDocument = require('pdfkit');

const Order = require('../database/models/order');
const cartService = require('./cart-service');

class OrdersService {
    createOrder(userId) {
        return cartService.getCart(userId)
            .then(cart => new Order({ userId, order: { ...cart } }).save())
            .then(() => cartService.clearCart(userId))
    }

    getOrders(userId) {
        return Order.find({ userId });
    }

    getInvoice(orderId, userId) {
        const invoiceName = `invoice_${orderId}.pdf`;
        const invoicePath = path.join('data', 'invoices', invoiceName);

        return Order.findById(orderId).then(order => {
            if (!order || !order.userId.equals(userId)) {
                return null;
            }

            const pdfDoc = new PDFDocument({ font: 'Courier' });

            //a READABLE stream ---> pipe it to WRITABLE stream
            //accept the path WHERE to write to
            //ensures that the pdf we generate on the fly is: 1) served on the server; 2) served to the user;
            pdfDoc.pipe(fs.createWriteStream(invoicePath));

            pdfDoc.fillColor('#1512ce').fontSize(18).text(`Invoice for order #${order._id}`);
            pdfDoc.text('----------');
            order.order.products.forEach(prod => {
                pdfDoc.fontSize(12).text(`${prod.title}: $${prod.price} x ${prod.amount}`);
            });
            pdfDoc.text('-----');
            pdfDoc.fontSize(14).text(`Total price, $: ${order.order.totalPrice}`);

            pdfDoc.end();

            return pdfDoc;
        });
    }
}

module.exports = new OrdersService();
