const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');

const transporter = nodemailer.createTransport(sendgridTransport({
    auth: { api_key: process.env.SEND_GRID_KEY }
}));

class MailerService {
    sendEmail(email, subject, html) {
        return transporter.sendMail({
            to: email,
            from: 'nodejs-course-angelina@outlook.com',
            subject,
            html
        });
    }
}

module.exports = new MailerService();
