const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const User = require('../database/models/user');

class AuthService {
    getUserByEmail(email) {
        return User.findOne({ email });
    }

    getUserByResetToken(resetToken) {
        return User.findOne({ resetToken, resetTokenExpiration: { $gt: Date.now() } });
    }

    signup(email, password) {
        return bcrypt.hash(password, 12).then(hashedPassword => {
            return new User({ email, password: hashedPassword, cart: [] }).save();
        });
    }

    login(email, password) {
        return User.findOne({ email }).then(user => {
            return bcrypt.compare(password, user.password).then(isPasswordValid => {
                return isPasswordValid ? user : null;
            });
        });
    }

    resetPassword(email) {
        const token_duration = 60 * 60 * 1000; // 36000000;
        let token = null;

        crypto.randomBytes(32, ((err, buffer) => {
            if (err) { return null; }
            token = buffer.toString('hex');
        }));

        return User.findOne({ email }).then(user => {
            if (!user) { return null; }

            user.resetToken = token;
            user.resetTokenExpiration = Date.now() + token_duration;

            return user.save();
        });
    }

    setNewPassword(userId, resetToken, password) {
        return User.findOne({ _id: userId, resetToken, resetTokenExpiration: { $gt: Date.now() } }).then(user => {
            return bcrypt.hash(password, 12).then(hashedPassword => {
                user.password = hashedPassword;
                user.resetToken = undefined;
                user.resetTokenExpiration = undefined;

                return user.save();
            });
        });
    }
}

module.exports = new AuthService();
