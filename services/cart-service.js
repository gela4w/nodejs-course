const User = require('../database/models/user');

class CartService {
    getCart(userId) {
        return User.findById(userId).then(user => {
            return user.populate('cart.productId').then(({ cart }) => {
                if (!cart) {
                    return null;
                }

                //cart is MONGOOSE object, convert it to plain JS object to be able to work with it
                cart = cart.toObject().map(({ productId, amount }) => ({ ...productId, amount }));
                const totalPrice = this.countTotalPrice(cart);

                return ({ products: cart, totalPrice });
            })
        })
    }

    addToCart(userId, prodId) {
        return User.findById(userId).then(user => {
            const cart = user.cart ?? [];
            const productInCart = cart.find(({ productId }) => productId?.toString() === prodId);

            if (productInCart) {
                ++productInCart.amount;
            } else {
                cart.push({ productId: prodId, amount: 1 });
            }

            user.cart = cart;

            return user.save();
        })
    }

    deleteFromCart(userId, prodId) {
        return User.findById(userId).then(user => {
            user.cart = user.cart.filter(({ productId }) => productId.toString() !== prodId);

            return user.save();
        })
    }

    clearCart(userId) {
        return User.findById(userId).then(user => {
            user.cart = [];

            return user.save();
        })
    }

    countTotalPrice(products) {
        return products
            .map(({ price, amount }) => price * amount)
            .reduce((curr, acc) => curr + acc, 0)
            .toFixed(2);
    }
}

module.exports = new CartService();
